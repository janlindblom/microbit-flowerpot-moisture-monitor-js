let new_value = 0
let position = 0
let value = 0
let screen = 0
let name = ""
let moisture: number[] = []
let screen_on = false

screen_on = true
radio.setGroup(67)
radio.setTransmitPower(1)
moisture = [0, 0, 0]

// Read from pins 0-2
function read_moisture() {
    moisture[0] = pins.analogReadPin(AnalogPin.P0)
    moisture[1] = pins.analogReadPin(AnalogPin.P1)
    moisture[2] = pins.analogReadPin(AnalogPin.P2)
}
// Setup everything
function setup() {
    read_moisture()
    plot_readings()
}
// Toggle "screen" output
input.onButtonPressed(Button.A, () => {
    screen_on = !(screen_on)
    transmit_screen_state()
    plot_readings()
})
// Send reading over radio
function transmit_readings() {
    for (let i = 0; i <= 2; i++) {
        name = "pot" + i
        radio.sendValue(name, moisture[i])
        toggle_radio_led()
    }
}
function transmit_screen_state()  {
    if (screen_on) {
        screen = 1
    } else {
        screen = 0
    }
    radio.sendValue("screen", screen)
}
// Plot readings to "screen"
function plot_readings() {
    if (screen_on) {
        for (let j = 0; j <= 2; j++) {
            value = moisture[j]
            position = j
            new_value = pins.map(
            value,
            0,
            1023,
            0,
            4
            )
            for (let y = 0; y <= 4; y++) {
                led.unplot(position, y)
            }
            for (let k = 0; k <= new_value; k++) {
                led.plot(position, 4 - k)
            }
        }
    } else {
        for (let x = 0; x <= 2; x++) {
            for (let z = 0; z <= 4; z++) {
                led.unplot(x, z)
            }
        }
    }
}
function toggle_radio_led() {
    if (screen_on) {
        led.plot(4, 4)
        basic.pause(500)
        led.unplot(4, 4)
    }
}

setup()
// Main loop
basic.forever(() => {
    basic.pause(1000)
    read_moisture()
    transmit_readings()
    plot_readings()
})
